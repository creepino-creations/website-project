$(function() {
    /**
     * Jquery code to insert header code into a blank <header> tag.
     * 
     * Using ejs now, this is not needed!
     */
    // $("header").load("/components/header");

    // Feather Icons
    setTimeout(() => feather.replace(), 500);
});